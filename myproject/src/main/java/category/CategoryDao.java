package category;

import config.ConnectionManager;
import expense.Expense;
import jakarta.persistence.EntityManager;

import java.util.List;

public class CategoryDao {

    public void insert(Category category) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(category);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Category> findAll() {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        List categories = entityManager.createQuery("select c from Category c").getResultList();
        entityManager.close();
        return categories;
    }

    public void deleteById(int categoryIdToBeDeleted) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Category c where c.id=:id")
                .setParameter("id", categoryIdToBeDeleted).executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
