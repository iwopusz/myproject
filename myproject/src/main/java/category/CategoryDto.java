package category;

import lombok.Data;


@Data
public class CategoryDto {

        private int categoryId;
        private String categoryName;

        public CategoryDto(int categoryId, String categoryName) {
                this.categoryId = categoryId;
                this.categoryName = categoryName;
        }

        public CategoryDto(String categoryName) {
                this.categoryName = categoryName;
        }
}
