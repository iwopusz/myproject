package category;

import config.ConnectionManager;
import expense.Expense;
import expense.ExpenseDao;
import expense.ExpenseDto;
import expense.PrintExpenseDto;
import jakarta.persistence.Column;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class CategoryService {

    private CategoryDao categoryDao;

    public void addCategory(CategoryDto categoryDto) throws IllegalArgumentException {
        if (categoryDto.getCategoryName().length() > 3) {
            Category category = new Category(categoryDto.getCategoryName());
            categoryDao.insert(category);
        } else {
            throw new IllegalArgumentException("Nazwa kategorii nie została podany");
        }
    }

    public List<PrintCategoryDto> getAllCategories() {
        List<Category> categories = categoryDao.findAll();
        return categories.stream().map(c -> new PrintCategoryDto(c.getCategoryId(), c.getCategoryName()))
                .collect(Collectors.toList());
    }

    public void deleteCategory(int categoryIdToBeDeleted) {
        if (categoryIdToBeDeleted > 0) {
            categoryDao.deleteById(categoryIdToBeDeleted);
        } else {
            throw new IllegalArgumentException("Id kategorii jest mniejsze bądź równe 0");
        }
    }
}
