package category;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PrintCategoryDto extends CategoryDto {

    private int id;

    public PrintCategoryDto(int categoryId, String categoryName) {
        super(categoryId, categoryName);
    }

    @Override
    public String toString() {
        return "Kategoria #" + getCategoryId() + " - " + getCategoryName();
    }
}
