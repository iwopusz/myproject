package expense;


import java.time.LocalDate;
import java.util.Date;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "expense")
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "amount")
    private Double amount;
    @Column(name = "comment")
    private String comment;
    @Column(name = "expense_add_date")
    private Date expenseAddDate;
    @Column(name = "category_id")
    private int categoryId;

    public Expense(Double amount, String comment, Date expenseAddDate, int categoryId) {
        this.amount = amount;
        this.comment = comment;
        this.expenseAddDate = expenseAddDate;
        this.categoryId = categoryId;
    }
}
