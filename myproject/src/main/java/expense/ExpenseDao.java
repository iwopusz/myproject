package expense;

import config.ConnectionManager;
import jakarta.persistence.EntityManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * data access object, przedrostek przed dao informuje użytkownika do jakiego obiektu chce uzyskać dostęp w DB.
 * W klasie dao piszemy wszystkie instrukcje, które komunikują się z DB.
 */
public class ExpenseDao {
    public void insert(Expense expense) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(expense);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Expense> findAll() {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        List expenses = entityManager.createQuery("select e from Expense e").getResultList();
        entityManager.close();
        return expenses;
    }

    public List<Expense> findAllByCategoryId(int categoryId) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        List expenses = entityManager.createQuery("select e from Expense e where e.categoryId=:categoryId")
                .setParameter("categoryId", categoryId)
                .getResultList();
        entityManager.close();
        return expenses;
    }

    public List<Expense> findAllByDate(String startDate, String endDate) throws ParseException {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = formatter.parse(startDate);
        Date date2 = formatter.parse(endDate);
        List expenses = entityManager.createQuery("select e from Expense e where e.expenseAddDate BETWEEN :startDate AND :endDate")
                .setParameter("startDate", date1)
                .setParameter("endDate", date2)
                .getResultList();
        entityManager.close();
        return expenses;
    }

    public void deleteById(int expenseIdToBeDeleted) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Expense e where e.id=:id")
                .setParameter("id", expenseIdToBeDeleted).executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
