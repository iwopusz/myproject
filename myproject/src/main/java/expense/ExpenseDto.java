package expense;

import category.Category;
import lombok.*;
import java.time.LocalDate;
import java.util.Date;

//@Data to to samo co: @Getter, @Setter, @ToString, @EqualsAndHashCode, @NoArgsConstructor
//DTO - data transfer object
@Data
@AllArgsConstructor
public class ExpenseDto {
    private Double amount;
    private String comment;

    private Date expenseAddDate;
    private int categoryId;


    public ExpenseDto(Double amount, String comment, int categoryId) {
        this.amount = amount;
        this.comment = comment;
        this.categoryId = categoryId;
    }
}