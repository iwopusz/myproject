package expense;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExpenseService {
    private ExpenseDao expenseDao;

    public void addExpense(ExpenseDto expenseDto) throws IllegalArgumentException {
        if (expenseDto.getAmount() != null && expenseDto.getComment().length() > 3) {
            Expense expense = new Expense(expenseDto.getAmount(), expenseDto.getComment(), expenseDto.getExpenseAddDate(), expenseDto.getCategoryId());
            expenseDao.insert(expense);
        } else {
            throw new IllegalArgumentException("Kwota lub opis wydatku nie został podany");
        }
    }

    public List<PrintExpenseDto> getAllExpenses() {
        List<Expense> expenses = expenseDao.findAll();
        return expenses.stream().map(e -> new PrintExpenseDto(e.getId(), e.getAmount(), e.getComment(), e.getExpenseAddDate(), e.getCategoryId()))
                .collect(Collectors.toList());
    }

    public List<PrintExpenseDto> getAllExpensesByCategoryId(int categoryId) {
        List<Expense> expenses = expenseDao.findAllByCategoryId(categoryId);
        return expenses.stream().map(e -> new PrintExpenseDto(e.getId(), e.getAmount(), e.getComment(), e.getExpenseAddDate(), e.getCategoryId()))
                .collect(Collectors.toList());
    }

    public List<PrintExpenseDto> getAllExpensesByDate(String startDate, String endDate) throws ParseException {
        List<Expense> expenses = expenseDao.findAllByDate(startDate, endDate);
        return expenses.stream().map(e -> new PrintExpenseDto(e.getId(), e.getAmount(), e.getComment(), e.getExpenseAddDate(), e.getCategoryId()))
                .collect(Collectors.toList());
    }

    public void deleteExpense(int expenseIdToBeDeleted) {
        if (expenseIdToBeDeleted > 0) {
            expenseDao.deleteById(expenseIdToBeDeleted);
        } else {
            throw new IllegalArgumentException("Id wydatku jest mniejsze bądź równe 0");
        }
    }

}
