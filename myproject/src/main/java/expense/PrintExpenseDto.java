package expense;

import java.time.LocalDate;
import java.util.Date;

import category.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrintExpenseDto extends ExpenseDto {

    private int id;
    private Date expenseAddDate;

    public PrintExpenseDto(int id, Double amount, String comment, Date expenseAddDate, int categoryId) {
        super(amount, comment, categoryId);
        this.id = id;
        this.expenseAddDate = expenseAddDate;
    }

    @Override
    public String toString() {
        return "Wydatek #" + id + " , kwota = " + getAmount() +
               ", opis - " + getComment() + " , dodany dnia " + expenseAddDate.toString() + ", w kategorii " + getCategoryId() + ".";
    }
}
