package income;


import java.time.LocalDate;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "income")
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "amount")
    private Double amount;
    @Column(name = "comment")
    private String comment;
    @Column(name = "income_add_date")
    private LocalDate incomeAddDate;


    public Income(Double amount, String comment, LocalDate incomeAddDate) {
        this.amount = amount;
        this.comment = comment;
        this.incomeAddDate = incomeAddDate;
    }
}
