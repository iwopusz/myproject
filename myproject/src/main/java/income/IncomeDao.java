package income;

import java.util.List;
import config.ConnectionManager;
import jakarta.persistence.EntityManager;

public class IncomeDao {
    public void insert(Income income) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(income);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Income> findAll() {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        List incomes = entityManager.createQuery("select e from Income e").getResultList();
        entityManager.close();
        return incomes;
    }

    public void deleteById(int incomeIdToBeDeleted) {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from Income e where e.id=:id")
                .setParameter("id", incomeIdToBeDeleted).executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
