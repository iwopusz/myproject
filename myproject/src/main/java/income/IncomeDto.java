package income;

import lombok.*;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class IncomeDto {
    private Double amount;
    private String comment;
    private LocalDate incomeAddDate;
}