package income;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class IncomeService {
    private IncomeDao incomeDao;

    public void addIncome(IncomeDto incomeDto) throws IllegalArgumentException {
        if (incomeDto.getAmount() != null && incomeDto.getComment().length() > 3) {
            Income income = new Income(incomeDto.getAmount(), incomeDto.getComment(), LocalDate.now());
            incomeDao.insert(income);
        } else {
            throw new IllegalArgumentException("Kwota lub opis przychodu nie został podany");
        }
    }

    public List<PrintIncomeDto> getAllIncomes() {
        List<Income> incomes = incomeDao.findAll();
        return incomes.stream().map(e -> new PrintIncomeDto(e.getId(), e.getAmount(), e.getComment(), e.getIncomeAddDate()))
                .collect(Collectors.toList());
    }

    public void deleteIncome(int incomeIdToBeDeleted) {
        if (incomeIdToBeDeleted > 0) {
            incomeDao.deleteById(incomeIdToBeDeleted);
        } else {
            throw new IllegalArgumentException("Id przychodu jest mniejsze bądź równe 0");
        }
    }
}
