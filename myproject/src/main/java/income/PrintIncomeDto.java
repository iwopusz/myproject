package income;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
public class PrintIncomeDto extends IncomeDto {

    private int id;

    public PrintIncomeDto(int id, Double amount, String comment, LocalDate incomeAddDate) {
        super(amount, comment, incomeAddDate);
        this.id = id;
    }


    @Override
    public String toString() {
        return "Przychód #" + getId() + " - " + getComment() + " - w kwocie " + getAmount() + " dodany " + getIncomeAddDate() + ".";
    }
}
