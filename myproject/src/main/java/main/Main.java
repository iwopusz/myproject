package main;

import java.text.ParseException;
import java.time.LocalDate;
import java.sql.Date;
import java.util.List;
import java.util.Scanner;
import category.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.ConnectionManager;
import income.IncomeDao;
import income.IncomeDto;
import income.IncomeService;
import income.PrintIncomeDto;
import jakarta.persistence.EntityManager;
import expense.PrintExpenseDto;
import expense.ExpenseDao;
import expense.ExpenseDto;
import expense.ExpenseService;
import sum.PrintSumDto;
import sum.SumDao;
import sum.SumService;

public class Main {

    //Wszystko zrobione na bazie projektu taskmanager prowadzącego
    //1. Użyłem wszędzie DAO/DTO. Nie wiem czy to dobrze, może trzeba na PreparedStatement albo NativeQuery?
    //2. Nie potrafię zaciągnąć nazwy kategorii zamiast id.

    public static void main(String[] args) throws ParseException {

        EntityManager entityManager = ConnectionManager.getEntityManager();
        entityManager.close();

        ExpenseDao expenseDao = new ExpenseDao();
        ExpenseService expenseService = new ExpenseService(expenseDao);
        IncomeDao incomeDao = new IncomeDao();
        IncomeService incomeService = new IncomeService(incomeDao);
        CategoryDao categoryDao = new CategoryDao();
        CategoryService categoryService = new CategoryService(categoryDao);
        SumDao sumDao = new SumDao();
        SumService sumService = new SumService(sumDao);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        while (true) {
            System.out.println("Podaj operację do wykonania:");
            System.out.println("0 - Zatrzymanie programu");
            System.out.println("1 - Dodawanie nowego wydatku");
            System.out.println("2 - Dodawanie nowego przychodu");
            System.out.println("3 - Usuwanie wydatku");
            System.out.println("4 - Usuwanie przychodu");
            System.out.println("5 - Wyświetlenie wszystkich wydatków i przychodów");
            System.out.println("6 - Wyświetlenie wszystkich wydatków");
            System.out.println("7 - Wyświetlenie wydatków na podstawie zakresu dat");
            System.out.println("8 - Wyświetlenie wszystkich wydatków na podstawie wybranej kategorii");
            System.out.println("9 - Wyświetlenie sumy wydatków oraz ich ilości w danej kategorii");
            System.out.println("10 - Wyświetlenie wszystkich przychodów");
            System.out.println("11 - Wyświetlenie Saldo");
            System.out.println("12 - Dodanie nowej kategorii");
            System.out.println("13 - Usunięcie istniejącej kategorii");
            System.out.println("20 - Wyświetlanie wszystkich kategorii");
            Scanner in = new Scanner(System.in);
            int selectedOperation = in.nextInt();
            switch (selectedOperation) {
                case 0 -> System.exit(0);
                case 1 -> {
                    System.out.println("Dodawanie nowego wydatku");
                    System.out.println("************************");
                    System.out.println("Podaj kategorię wydatku:");
                    List<PrintCategoryDto> categories = categoryService.getAllCategories();
                    categories.forEach(System.out::println);
                    int categoryId = in.nextInt();
                    System.out.println("Podaj kwotę wydatku:");
                    Double amount = in.nextDouble();
                    System.out.println("Podaj opis wydatku");
                    in.nextLine();
                    String comment = in.nextLine();
                    Date expenseAddDate = java.sql.Date.valueOf(LocalDate.now());
                    ExpenseDto expenseDto = new ExpenseDto(amount, comment, expenseAddDate, categoryId);
                    try {
                        expenseService.addExpense(expenseDto);
                        System.out.println("Wydatek został dodany");
                    } catch (IllegalArgumentException e) {
                        System.err.println(e.getMessage());
                    }
                }
                case 2 -> {
                    System.out.println("Dodawanie nowego przychodu");
                    System.out.println("************************");
                    System.out.println("Podaj kwotę przychodu:");
                    Double amount = in.nextDouble();
                    System.out.println("Podaj opis przychodu");
                    in.nextLine();
                    String comment = in.nextLine();
                    IncomeDto incomeDto = new IncomeDto(amount, comment, LocalDate.now());
                    try {
                        incomeService.addIncome(incomeDto);
                        System.out.println("Przychód został dodany");
                    } catch (IllegalArgumentException e) {
                        System.err.println(e.getMessage());
                    }
                }
                case 3 -> {
                    System.out.println("Usuwanie istniejącego wydatku");
                    System.out.println("*****************************");
                    System.out.println("Podaj id wydatku, który chcesz usunąć: ");
                    List<PrintExpenseDto> expenses = expenseService.getAllExpenses();
                    expenses.forEach(System.out::println);
                    int expenseIdToBeDeleted = in.nextInt();
                    expenseService.deleteExpense(expenseIdToBeDeleted);
                }
                case 4 -> {
                    System.out.println("Usuwanie przychodu");
                    System.out.println("*********************************");
                    System.out.println("Podaj id przychodu, który chcesz usunąć: ");
                    List<PrintIncomeDto> incomes = incomeService.getAllIncomes();
                    incomes.forEach(System.out::println);
                    int incomeIdToBeDeleted = in.nextInt();
                    incomeService.deleteIncome(incomeIdToBeDeleted);
                }
                case 5 -> {
                    System.out.println("Wyświetlenie wszystkich wydatków i przychodów");
                    System.out.println("*********************************************");
                    System.out.println("******************PRZYCHODY******************");
                    List<PrintIncomeDto> incomes = incomeService.getAllIncomes();
                    incomes.forEach(System.out::println);
                    System.out.println("******************WYDATKI*********************");
                    List<PrintExpenseDto> expenses = expenseService.getAllExpenses();
                    expenses.forEach(System.out::println);
                }
                case 6 -> {
                    System.out.println("Wyświetlanie wszystkich wydatków");
                    System.out.println("********************************");
                    List<PrintExpenseDto> expenses = expenseService.getAllExpenses();
                    // do VM options trzeba dodać "--add-opens java.base/java.time=ALL-UNNAMED"
                    //System.out.println(gson.toJson(tasks));
                    expenses.forEach(System.out::println);
                }
                case 7 -> {
                    System.out.println("Wyświetlenie wydatków na podstawie zakresu dat");
                    System.out.println("**********************************************");
                    System.out.println("PODAJ ZAKRES DAT");
                    System.out.println("Początek RRRR-MM-DD:");
                    in.nextLine();
                    String startDate = in.nextLine();
                    System.out.println("Koniec RRRR-MM-DD:");
                    String endDate = in.nextLine();
                    System.out.println("******************WYDATKI*********************");
                    List<PrintExpenseDto> expenses = expenseService.getAllExpensesByDate(startDate, endDate);
                    expenses.forEach(System.out::println);
                }
                case 8 -> {
                    System.out.println("Wyświetlenie wszystkich wydatków na podstawie wybranej kategorii");
                    System.out.println("****************************************************************");
                    System.out.println("Podaj id kategorii wydatków, którą chcesz wyświetlić:");
                    List<PrintCategoryDto> categories = categoryService.getAllCategories();
                    categories.forEach(System.out::println);
                    int categoryIdOfExpenses = in.nextInt();
                    List<PrintExpenseDto> expenses = expenseService.getAllExpensesByCategoryId(categoryIdOfExpenses);
                    expenses.forEach(System.out::println);
                }
                case 9 -> {
                    System.out.println("Wyświetlenie sumy wydatków oraz ich ilości w danej kategorii");
                    System.out.println("************************************************************");
                    List<PrintSumDto> expenses = sumService.getSumByCategory();
                    expenses.forEach(System.out::println);
                }
                case 10 -> {
                    System.out.println("Wyświetlenie wszystkich przychodów");
                    System.out.println("**********************************");
                    List<PrintIncomeDto> incomes = incomeService.getAllIncomes();
                    incomes.forEach(System.out::println);
                }
                case 11 -> {
                    System.out.println("Wyświetlenie Saldo");
                    System.out.println("******************");
                    System.out.println("Aktualne Saldo wynosi:");
                    sumService.getSaldo();
                }
                case 12 -> {
                    System.out.println("Dodanie nowej kategorii");
                    System.out.println("************************");
                    System.out.println("Podaj nazwę nowej kategorii:");
                    in.nextLine();
                    String newCategoryName = in.nextLine();
                    CategoryDto categoryDto = new CategoryDto(newCategoryName);
                    try {
                        categoryService.addCategory(categoryDto);
                        System.out.println("Kategoria została dodana");
                    } catch (IllegalArgumentException e) {
                        System.err.println(e.getMessage());
                    }
                }
                case 13 -> {
                    System.out.println("Usunięcie istniejącej kategori");
                    System.out.println("******************************");
                    System.out.println("Podaj id kategorii, którą chcesz usunąć:");
                    List<PrintCategoryDto> categories = categoryService.getAllCategories();
                    categories.forEach(System.out::println);
                    int categoryIdToBeDeleted = in.nextInt();
                    categoryService.deleteCategory(categoryIdToBeDeleted);
                }
                case 20 -> {
                    System.out.println("Wyświetlanie wszystkich kategorii");
                    System.out.println("*********************************");
                    List<PrintCategoryDto> categories = categoryService.getAllCategories();
                    categories.forEach(System.out::println);
                }
            }
        }
    }
}


//        a. Opcjonalnie można podać zakres dat np. zakres dat bieżącego miesiąca do SALDO
