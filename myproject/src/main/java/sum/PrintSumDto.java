package sum;

import category.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrintSumDto extends SumDto {

    public PrintSumDto(Double amount, int categoryId) {
        super(amount, categoryId);
            }

    @Override
    public String toString() {
        return "Kategoria #" + getCategoryId() + " , kwota = " + getAmount() + ".";
    }


}
