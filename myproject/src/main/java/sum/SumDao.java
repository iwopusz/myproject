package sum;

import config.ConnectionManager;
import expense.Expense;
import jakarta.persistence.EntityManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class SumDao {

    public double getSaldo() {
        double incomesSum = 0.0;
        double expensesSum = 0.0;
        double saldo = 0.0;
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/financemanagerdb", "sda_reader", "12345");
            PreparedStatement statement1 = connection.prepareStatement("select sum(amount) from expense");
            PreparedStatement statement2 = connection.prepareStatement("select sum(amount) from income");
            ResultSet result1 = statement1.executeQuery();
            result1.next();
            expensesSum = result1.getDouble(1);
            ResultSet result2 = statement2.executeQuery();
            result2.next();
            incomesSum = result2.getDouble(1);
            saldo = incomesSum - expensesSum;
            System.out.println(saldo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return saldo;
    }

    public List<Expense> getSumByCategory() {
        EntityManager entityManager = ConnectionManager.getEntityManager();
        List expenses = entityManager.createQuery("select e from Expense e group by categoryId").getResultList();
        entityManager.close();
        return expenses;
    }
}
