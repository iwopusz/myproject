package sum;


import lombok.Data;

@Data
public class SumDto {

    private Double amount;
    private int categoryId;

    public SumDto(Double amount, int categoryId) {
        this.amount = amount;
        this.categoryId = categoryId;
    }
}
