package sum;

import expense.Expense;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class SumService {

    private SumDao sumDao;

    public void getSaldo() {
        sumDao.getSaldo();
    }

    public List<PrintSumDto> getSumByCategory() {
        List<Expense> expenses = sumDao.getSumByCategory();
        return expenses.stream().map(e -> new PrintSumDto(e.getAmount(), e.getCategoryId()))
                .collect(Collectors.toList());
    }
}

